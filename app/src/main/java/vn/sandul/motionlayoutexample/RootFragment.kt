package vn.sandul.motionlayoutexample

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_root.*

class RootFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_root, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnFromTopToBottom.setOnClickListener {
            findNavController().navigate(R.id.action_rootFragment_to_moveFromTopToBottomFragment)
        }

        btnFromLeftToRight.setOnClickListener {
            findNavController().navigate(R.id.action_rootFragment_to_moveFromLeftToRightFragment)
        }

        btnFromLeftToRightAndChangeColor.setOnClickListener {
            findNavController().navigate(R.id.action_rootFragment_to_moveFromLeftToRightAndChangeColorFragment)
        }

        btnFromLeftToRightAndChangeColorAndKeyFrameSet.setOnClickListener {
            findNavController().navigate(R.id.action_rootFragment_to_moveFromLeftToRightAndChangeColorAndKeyFrameSetFragment)
        }

        btnMoveAll.setOnClickListener {
            findNavController().navigate(R.id.action_rootFragment_to_moveAllFragment)
        }
    }
}