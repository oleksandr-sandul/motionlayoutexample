package vn.sandul.motionlayoutexample.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_move_all.*
import vn.sandul.motionlayoutexample.R

class MoveAllFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_move_all, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnRun.setOnClickListener {
            when (motionLayout.currentState) {
                R.id.start -> motionLayout.transitionToEnd()
                R.id.end -> motionLayout.transitionToStart()
            }
        }
    }
}